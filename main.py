import sys
import csv
import pandas as pd
from PyQt6.QtWidgets import QApplication, QMainWindow, QTabWidget, QGridLayout, QWidget, QLabel, QPushButton, QFileDialog, QListWidget, QMessageBox
from dataclasses import dataclass


@dataclass
class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Z-S Overview Customizer")
        self.setGeometry(100, 100, 1280, 800)

        # self invTypes, invGroups and unifiedLists lists
        self.invTypes_list = []
        self.invGroups_list = []
        self.unified_list = []

        tab_widget = QTabWidget(self)
        self.setCentralWidget(tab_widget)

        # First tab: Data Dump Editor
        datadump_editor = QWidget()
        tab_widget.addTab(datadump_editor, "Data Dump Editor")
        layout_datadump_editor = QGridLayout(datadump_editor)

        convert_invTypes_button = QPushButton(
            "Convert invTypes XLS to CSV and trim")
        import_invTypes_button = QPushButton("Import invTypes CSV")
        import_invGroups_button = QPushButton("Import invGroups CSV")
        unify_lists_button = QPushButton("Unify Lists")

        # tooltip over the buttons
        convert_invTypes_button.setToolTip(
            "Converts the 'invTypes.xls' file provided by fuzzworks' game dump to CSV, and trims the file to the first 3 columns.")
        import_invTypes_button.setToolTip(
            "Imports the 'invTypes.csv' into the list below.")
        import_invGroups_button.setToolTip(
            "Imports the 'invGroups.csv' into the list below.")
        unify_lists_button.setToolTip(
            "Unify the lists to match groupID's with groupNames.")

        convert_invTypes_button.clicked.connect(self.convert_xls_csv)
        layout_datadump_editor.addWidget(convert_invTypes_button, 1, 0, 1, 1)

        import_invTypes_button.clicked.connect(self.import_invTypes_csv)
        layout_datadump_editor.addWidget(import_invTypes_button, 2, 0, 1, 1)

        import_invGroups_button.clicked.connect(self.import_invGroups_csv)
        layout_datadump_editor.addWidget(import_invGroups_button, 2, 1, 1, 1)

        unify_lists_button.clicked.connect(self.import_unify_lists)
        layout_datadump_editor.addWidget(unify_lists_button, 2, 2, 1, 1)

        self.list_invTypes = QListWidget()
        layout_datadump_editor.addWidget(self.list_invTypes, 3, 0, 1, 1)

        self.list_invGroups = QListWidget()
        layout_datadump_editor.addWidget(self.list_invGroups, 3, 1, 1, 1)

        self.list_unified = QListWidget()
        layout_datadump_editor.addWidget(self.list_unified, 3, 2, 1, 1)

        # Second tab: Overview Settings
        overview_settings = QWidget()
        tab_widget.addTab(overview_settings, "Overview Settings")
        layout_overview_settings = QGridLayout(overview_settings)

        # Tabs of the overview settings
        overview_settings_tabs = QTabWidget()
        for i, name in enumerate(["Overview Tabs", "Tab Presets", "Appearance", "Columns", "Ships", "Misc", "History"]):
            tab = QWidget()
            label = QLabel(name)
            layout = QGridLayout(tab)
            layout.addWidget(label, 0, 0)
            overview_settings_tabs.addTab(tab, name)
        layout_overview_settings.addWidget(overview_settings_tabs, 0, 0, 1, 1)

        live_view = QLabel("Live View")
        layout_overview_settings.addWidget(live_view, 0, 1, 1, 1)

        # Third tab: Preset Editor
        preset_editor = QWidget()
        tab_widget.addTab(preset_editor, "Preset Editor")
        layout_preset_editor = QGridLayout(preset_editor)
        label1 = QLabel("Column 1")
        label2 = QLabel("Column 2")
        label3 = QLabel("Column 3")
        layout_preset_editor.addWidget(label1, 0, 0)
        layout_preset_editor.addWidget(label2, 0, 1)
        layout_preset_editor.addWidget(label3, 0, 2)

        # Fourth tab: Tab Editor
        tab_editor = QWidget()
        tab_widget.addTab(tab_editor, "Tab Editor")
        layout_tab_editor = QGridLayout(tab_editor)
        label1 = QLabel("Column 1")
        label2 = QLabel("Column 2")
        label3 = QLabel("Column 3")
        layout_tab_editor.addWidget(label1, 0, 0)
        layout_tab_editor.addWidget(label2, 0, 1)
        layout_tab_editor.addWidget(label3, 0, 2)

    def convert_xls_csv(self):
        file_name, _ = QFileDialog.getOpenFileName(
            self, "Import .xls File", "", "XLS Files (*.xls);;All Files (*)")
        if file_name:
            try:
                df = pd.read_excel(file_name)

                # keep the first 3 columns
                df = df.iloc[:, :3]

                """ # make the first row the column headers, as strings
                df.columns = df.iloc[0].astype(str)

                # make the first and second column, from the second row onwards, values all integers
                df.iloc[1:, :2] = df.iloc[1:, :2].astype(int)

                # make the third column values all strings
                df.iloc[1:, 2] = df.iloc[1:, 2].astype(str) """

                csv_file = file_name.replace(".xls", ".csv")
                df.to_csv(csv_file, index=False)
                QMessageBox.information(
                    self, "Success", "File converted successfully!")
            except Exception as e:
                QMessageBox.critical(self, "Error", str(e))

    def import_invTypes_csv(self):
        file_name, _ = QFileDialog.getOpenFileName(
            self, "Import invTypes CSV", "", "CSV Files (*.csv)")
        if file_name:
            try:
                with open(file_name, 'r') as file:
                    invTypes_data = csv.reader(file)
                    for row in invTypes_data:
                        self.list_invTypes.addItem(str(row))
                        self.invTypes_list.append(row)
            except Exception as e:
                QMessageBox.critical(self, "Error", str(e))
                raise e
            else:
                QMessageBox.information(
                    self, "Success", "File imported successfully.")
        else:
            QMessageBox.warning(self, "Cancelled", "Import cancelled.")

    def import_invGroups_csv(self):
        file_name, _ = QFileDialog.getOpenFileName(
            self, "Import invGroups CSV", "", "CSV Files (*.csv)")
        if file_name:
            try:
                with open(file_name, 'r') as file:
                    invGroups_data = csv.reader(file)
                    # delete the second column and save as a list
                    result = []
                    for row in invGroups_data:
                        first_col = row[0]
                        third_col = row[2]
                        result.append([first_col, third_col])
                    for row in result:
                        self.list_invGroups.addItem(str(row))
                        self.invGroups_list.append(row)
            except Exception as e:
                QMessageBox.critical(self, "Error", str(e))
                raise e
            else:
                QMessageBox.information(
                    self, "Success", "File imported successfully.")
        else:
            QMessageBox.warning(self, "Cancelled", "Import cancelled.")

    def import_unify_lists(self):
        try:
            for i in self.invTypes_list:
                for j in self.invGroups_list:
                    if i[1] == j[0]:
                        self.unified_list.append([i[0], i[1], j[1], i[2]])
        except Exception as e:
            QMessageBox.critical(self, "Error", str(e))
            raise e
        else:
            QMessageBox.information(
                self, "Success", "Lists unified successfully.")
            for row in self.unified_list:
                self.list_unified.addItem(str(row))


if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    sys.exit(app.exec())
